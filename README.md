Hier sind die Notizen des Professors Grohs zur Vorlesung
[Lineare Algebra 2](https://ufind.univie.ac.at/de/course.html?lv=250082&semester=2022S)
an der Universität Wien im Sommersemester 2022.

Der LaTeX-Satz ist von mir (Anton Mosich).
