{
  description = "BSc LinAlg2 Mitschrift";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
  };

  outputs =
    { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      tex = (
        pkgs.texlive.combine {
          inherit (pkgs.texlive)
            scheme-medium
            latexmk # My favorite tool for compiling
            babel-german
            booktabs
            eso-pic
            faktor
            koma-script
            listings
            marvosym
            mathdots
            mathpunctspace
            mathtools
            metafont
            cancel # Cancelling out stuff
            cleveref # References with automatic names
            collection-fontsextra # additional fonts
            enumitem # fancier enumeration
            environ # Needed for hiding proofs
            forloop # Basically what the name says, repeated code
            framed # Frames around envs
            gitinfo2 # Including commit hash in compiled PDF
            etoolbox
            harpoon # "Vector" arrow above glyphs
            import # Used for importing svgs in LaTeX
            lettrine # Fancy letters in the beginning of a paragraph
            lipsum # lorem ipsum text
            listingsutf8 # Importing code files
            minifp # Fixed point calculations, dependency for something else
            ntheorem # Fancy theorem environments
            pdfjam # Resize PDFs to other paper sizes
            pgfplots # Easy graphs of functions
            tcolorbox # colored boxes for pretty environments
            tikzmark # markings in tikz images
            verse # Prettier verse spacing
            xstring # String manipulation etc, dependency for something else
            ;
        }
      );
    in
    {
      packages."x86_64-linux".default = pkgs.stdenvNoCC.mkDerivation rec {
        src = self;
        name = "LinAlg2";
        buildInputs = with pkgs; [
          gawk
          gnuplot
          tex
          coreutils
          findutils
        ];
        buildPhase = ''
          export HOME=.
          export PATH="${pkgs.lib.makeBinPath buildInputs}";
          mkdir -p .cache/texmf-var
          env TEXMFHOME=.cache TEXMFVAR=.cache/texmfvar \
            latexmk -pdf -f
        '';
        installPhase = ''
          mkdir -p $out
          cp LinAlg2.pdf $out/
        '';
      };
    };
}
